﻿using System;
using System.IO;
using System.Reflection;

namespace Michaelkc.ExPrint
{
    internal class ApplicationConfiguration
    {
        public string[] ProcessesToProbe { get; set; }
        public string[] ExceptionsToIgnore { get; set; }
        public string EntryExe { get; private set; }
        public string ProgramVersion { get; private set; }

        internal static ApplicationConfiguration CreateFromConfig()
        {
            var config = new ApplicationConfiguration()
            {
                ExceptionsToIgnore = System.Configuration.ConfigurationManager.AppSettings["ExceptionsToIgnore"].Split(','),
                ProcessesToProbe = System.Configuration.ConfigurationManager.AppSettings["ProcessesToProbe"].Split(','),
                EntryExe = Path.GetFileName(Assembly.GetEntryAssembly().Location),
                ProgramVersion = Assembly.GetEntryAssembly().GetName().Version.ToString(),
        };
            return config;
        }
    }
}
