﻿using Michaelkc.ExPrint;
using System;

namespace Michaelkc.ExPrint
{
    internal class RunConfiguration
    {
        private RunConfiguration()
        {}
        public RunMode CurrentMode { get; internal set; }
        public string SearchString { get; private set; }

        internal static RunConfiguration CreateFromArgs(string[] args)
        {
            var runMode = RunMode.DisplayProcesses;
            var searchString = string.Empty;
            if (args?.Length == 1)
            {
                runMode = RunMode.AttachToProcess;
                searchString = args[0];
            }
            return new RunConfiguration
            {
                CurrentMode = runMode,
                SearchString = searchString
            };
        }
    }
}