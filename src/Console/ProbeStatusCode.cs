﻿namespace Michaelkc.ExPrint
{
    internal enum ProbeStatusCode
    {
        OK,
        NotProbed,
        AccessDenied,
        Timeout,
        Error
    }
}