﻿using System;
using System.Linq;
using System.Reflection;
using System.Threading;
using log4net;
using Microsoft.Samples.Debugging.MdbgEngine;

namespace Michaelkc.ExPrint
{
    internal enum RunMode
    {
        DisplayProcesses,
        AttachToProcess
    }
}