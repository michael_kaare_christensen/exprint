﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Michaelkc.ExPrint
{
    internal class ProcessFinder
    {
        private readonly ManagedProcessRepository _managedProcessRepository;

        public ProcessFinder(ManagedProcessRepository managedProcessRepository)
        {
            _managedProcessRepository = managedProcessRepository;
        }

        public List<ManagedProcess> FindByName(string searchString)
        {
            var processes = _managedProcessRepository.GetAll().Where(p => p.Id != System.Diagnostics.Process.GetCurrentProcess().Id).ToList();
            if (searchString == null)
            {
                return processes;
            }

            uint pid;
            if (uint.TryParse(searchString, out pid))
            {
                return processes.Where(p => p.Id == pid).ToList();
            }

            var matchingProcesses = new List<ManagedProcess>();
            foreach (var process in processes)
            {
                if (process.DisplayName.Contains(searchString, StringComparison.OrdinalIgnoreCase))
                {
                    matchingProcesses.Add(process);
                }
                else
                {
                    if (process.ProbeResult.AppDomains.Any(appDomain => appDomain.DisplayName.Contains(searchString, StringComparison.OrdinalIgnoreCase)))
                    {
                        matchingProcesses.Add(process);
                    }
                }
            }
            return matchingProcesses;
        }
    }
}