﻿using System.Collections.Generic;

namespace Michaelkc.ExPrint
{
    internal class ProbeResult
    {
        public ProbeResult(ProbeStatusCode probeStatusCode, IEnumerable<ManagedAppDomain> appDomains)
        {
            ProbeStatusCode = probeStatusCode;
            AppDomains = appDomains;
            ErrorMessage = string.Empty;
        }

        public ProbeResult(ProbeStatusCode probeStatusCode, string errorMessage)
        {
            ProbeStatusCode = probeStatusCode;
            AppDomains = new ManagedAppDomain[0];
            ErrorMessage = errorMessage;
        }

        public string ErrorMessage { get; set; }
        public ProbeStatusCode ProbeStatusCode { get; set; }
        public IEnumerable<ManagedAppDomain> AppDomains { get; private set; }
    }
}