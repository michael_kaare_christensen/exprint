using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using Microsoft.Samples.Debugging.CorDebug;
using Microsoft.Samples.Debugging.MdbgEngine;

namespace Michaelkc.ExPrint
{
    // Adapted from http://www.bearcanyon.com/dotnet/#corpub
    internal class ManagedProcessRepository
    {
        private readonly ApplicationConfiguration _configuration;

        public ManagedProcessRepository(ApplicationConfiguration configuration)
        {
            _configuration = configuration;
        }

        private ProbeResult ProbeWithDebugger(int processId, CLRRuntimeInfo[] runtimes)
        {
            MDbgProcess mdbgProcess = null;
            try
            {
                // Intialize a MDbgEngine instance. 
                MDbgEngine engine = new MDbgEngine();

                //Attach a debugger to the specified process, and 
                //return a MDbgProcess instance that represents the Process.
                if (runtimes.Count() == 1)
                {
                    mdbgProcess = engine.Attach(processId,
                        runtimes.First().GetVersionString());
                }
                else
                {
                    mdbgProcess = engine.Attach(processId,
                        CorDebugger.GetDefaultDebuggerVersion());
                }


                // Wait 2 seconds if the process is not in Synchronized state.
                // One process can only be attached when it is in Synchronized state.
                // See http://msdn.microsoft.com/en-us/library/ms404528.aspx
                bool result = mdbgProcess.Go().WaitOne(2000);

                if (!result)
                {
                    return new ProbeResult(ProbeStatusCode.Timeout, string.Format(@"The process with an ID {0} could not be attached. Operation time out.", processId));
                }

                var appDomains = mdbgProcess.CorProcess.AppDomains.Cast<CorAppDomain>()
                .Select(ad => new ManagedAppDomain() { DisplayName = ad.Name, Id = (uint)ad.Id }).ToArray();


                return new ProbeResult(ProbeStatusCode.OK, appDomains);

            }
            catch (COMException)
            {
                return new ProbeResult(ProbeStatusCode.AccessDenied, string.Format(@"The process with an ID {0} could not be attached. Access is denied or it has already been attached.", processId));
            }
            catch (Exception ex)
            {
                return new ProbeResult(ProbeStatusCode.Error, string.Format(@"The process with an ID {0} could not be attached: {1}", processId,ex.Message));
                //throw;
            }
            finally
            {
                if (mdbgProcess != null)
                {
                    mdbgProcess.Detach();
                }
            }
        }


        /// <summary>
        /// Gets all managed processes. 
        /// </summary>
        public List<ManagedProcess> GetAll()
        {
            List<ManagedProcess> managedProcesses = new List<ManagedProcess>();

            // CLRMetaHost implements ICLRMetaHost Interface which provides a method that
            // return list all runtimes that are loaded in a specified process.
            CLRMetaHost host = new CLRMetaHost();

            var processes = System.Diagnostics.Process.GetProcesses();

            foreach (System.Diagnostics.Process diagnosticsProcess in processes)
            {
                try
                {

                    // Lists all runtimes that are loaded in a specified process.
                    var runtimes = new CLRRuntimeInfo[0];
                    var candidateRuntimes = host.EnumerateLoadedRuntimes(diagnosticsProcess.Id);
                    if (candidateRuntimes != null)
                    {
                        runtimes = candidateRuntimes.ToArray();
                    }
                    // If the process loads CLRs, it could be considered as a managed process.
                    if (runtimes.Any())
                    {
                        var probeResult = new ProbeResult(ProbeStatusCode.NotProbed,"");
                        if (IsSafeForProbing(diagnosticsProcess))
                        {
                            probeResult = ProbeWithDebugger(diagnosticsProcess.Id, runtimes);
                        }

                        managedProcesses.Add(new ManagedProcess((uint) diagnosticsProcess.Id,diagnosticsProcess.ProcessName, probeResult));
                    }

                }

                // The method EnumerateLoadedRuntimes will throw Win32Exception when the 
                // file cannot be found or access is denied. For example, the 
                // diagnosticsProcess is System or System Idle Process.
                catch (Win32Exception)
                { }

                // The method EnumerateLoadedRuntimes will throw COMException when it tries
                // to access a 64bit process on 64bit OS if this application is built on 
                // platform x86.
                catch (COMException)
                { }

                // Rethrow other exceptions.
                catch
                {
                    throw;
                }
            }
            return managedProcesses;
        }

        private bool IsSafeForProbing(Process diagnosticsProcess)
        {
            return (_configuration.ProcessesToProbe.Contains(diagnosticsProcess.ProcessName));
        }
    }
}