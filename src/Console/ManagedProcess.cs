namespace Michaelkc.ExPrint
{
    internal class ManagedProcess
    {
        public ManagedProcess(uint id, string displayName, ProbeResult probeResult)
        {
            Id = id;
            DisplayName = displayName;
            ProbeResult = probeResult;
        }

        public ManagedProcess()
        {
        }

        public uint Id { get; set; }
        public string DisplayName { get; set; }
        public ProbeResult ProbeResult { get; set; }
    }
}