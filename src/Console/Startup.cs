﻿using Autofac;
using log4net;
using System;
using System.Linq;
using System.Reflection;

namespace Michaelkc.ExPrint
{
    internal class Startup
    {
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        private static void Main(string[] args)
        {
            try
            {
                var container = BuildContainer(args);
                var program = container.Resolve<Program>();
                program.Run();

            }
            catch (Exception ex)
            {
                Console.WriteLine("Abnormal program termination. Consider restarting debuggee");
                Log.Fatal(ex);
                throw;
            }

        }

        private static IContainer BuildContainer(string[] args)
        {
            var builder = new ContainerBuilder();
            builder
                .RegisterAssemblyTypes(Assembly.GetExecutingAssembly())
                .Except<ApplicationConfiguration>()
                .Except<RunConfiguration>()
                .AsImplementedInterfaces()
                .AsSelf()
                .SingleInstance();

            builder.RegisterInstance(ApplicationConfiguration.CreateFromConfig());
            builder.RegisterInstance(RunConfiguration.CreateFromArgs(args));
            return builder.Build();
        }
    }
}