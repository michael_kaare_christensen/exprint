﻿using Microsoft.Samples.Debugging.MdbgEngine;
using System;
using System.Linq;

namespace Michaelkc.ExPrint
{
    internal class ExceptionModel
    {
        private ExceptionModel()
        { }

        public string Path { get; set; }
        public string Exception { get; set; }
        public string ExceptionType { get; set; }
        public ExceptionThrownStopReason StopReason { get; set; }
        public string Callstack { get; set; }

        public static ExceptionModel CreateFromProcess(MDbgProcess proc, ExceptionThrownStopReason stopReason)
        {
            MDbgThread activeThreads = proc.Threads.Active;
            MDbgValue currentException = activeThreads.CurrentException;
            var exceptionModel = new ExceptionModel
            {
                Path = "<no path>",
                Exception = "<no exception>",
                Callstack = "<no function name>",
                StopReason = stopReason
                ,
            };

            if (activeThreads != null && activeThreads.CurrentSourcePosition != null)
            {
                exceptionModel.Path = string.Format("{0}:{1}", activeThreads.CurrentSourcePosition.Path,
                    activeThreads.CurrentSourcePosition.Line);
            }

            if (currentException != null)
            {
                string msg = "<unknown>";
                try
                {
                    msg = currentException.GetField("_message").GetStringValue(false);
                }
                catch (Exception)
                {
                }
                exceptionModel.ExceptionType = currentException.TypeName;
                exceptionModel.Exception = currentException.TypeName + Environment.NewLine + msg;


            }

            if (activeThreads.Frames != null)
            {
                var frames = activeThreads.Frames.Cast<MDbgFrame>();
                foreach (var frame in frames)
                {
                    if (frame.IsManaged)
                    {
                        exceptionModel.Callstack += string.Format(" at {0}{1}", frame.Function.FullName, Environment.NewLine);
                    }
                }
            }
            return exceptionModel;
        }
    }
}