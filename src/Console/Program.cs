﻿using System;
using log4net;
using log4net.Appender;
using log4net.Layout;
using log4net.Repository.Hierarchy;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Michaelkc.ExPrint
{
    internal class Program
    {
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private RunConfiguration _runConfiguration;
        private ApplicationConfiguration _applicationConfiguration;
        private ProcessFinder _processFinder;
        private ExceptionDumper _exceptionDumper;

        public Program(ApplicationConfiguration applicationConfiguration, RunConfiguration runConfiguration, ProcessFinder processFinder, ExceptionDumper exceptionDumper)
        {
            _runConfiguration = runConfiguration;
            _applicationConfiguration = applicationConfiguration;
            _processFinder = processFinder;
            _exceptionDumper = exceptionDumper;
        }

        internal void Run()
        {
            ConfigureLog4net();
            DisplayBanner();
            switch (_runConfiguration.CurrentMode)
            {
                case RunMode.DisplayProcesses:
                    DisplayHelp();
                    DisplayAllProcesses();
                    break;
                case RunMode.AttachToProcess:
                    AttachToProcess();
                    break;
            }
        }

        private void DisplayBanner()
        {
            Log.Info(string.Format("{0} version {1}", _applicationConfiguration.EntryExe, _applicationConfiguration.ProgramVersion));
            Log.Info("Copyright 2016 Michael Christensen. BSD license (2-clause)");
        }

        private void AttachToProcess()
        {
            IEnumerable<ManagedProcess> processes;
            processes = _processFinder.FindByName(_runConfiguration.SearchString);
            if (!processes.Any())
            {
                Log.Warn("No processes matched");
                return;
            }
            if (processes.Skip(1).Any())
            {
                Log.Warn("More than a single process matched:");
                DisplayProcesses(processes);
                return;
            }
            Log.Info("Found single process:");
            DisplayProcesses(processes);

            var process = processes.Single();
            try
            {
                _exceptionDumper.Attach((int)process.Id);
                Log.Info("Press any key to detach");
                while (_exceptionDumper.CanContinue() && ShouldContinue())
                {
                    _exceptionDumper.DumpNextExceptionWithTimeout(TimeSpan.FromSeconds(3));
                }
            }
            finally
            {
                _exceptionDumper.Detach();
            }
        }

        private void DisplayAllProcesses()
        {
            IEnumerable<ManagedProcess> processes;
            processes = _processFinder.FindByName(null);
            Log.Info("Managed processes available for attaching:");
            DisplayProcesses(processes);
        }

        private void DisplayProcesses(IEnumerable<ManagedProcess> processes)
        {
            foreach (var process in processes)
            {
                Log.InfoFormat("{0} ({1})", process.DisplayName, process.Id);

                switch (process.ProbeResult.ProbeStatusCode)
                {
                    case ProbeStatusCode.OK:
                        {
                            foreach (var appDomain in process.ProbeResult.AppDomains)
                            {
                                Log.InfoFormat("\t{0} ({1})", appDomain.DisplayName, appDomain.Id);
                            }
                            break;
                        }

                    case ProbeStatusCode.NotProbed:
                        {
                            break;
                        }
                    default:
                        {
                            Log.InfoFormat("\t{0} - {1}", process.ProbeResult.ProbeStatusCode,
                                process.ProbeResult.ErrorMessage);
                            break;
                        }
                }
            }
        }

        private void DisplayHelp()
        {
            Log.WarnFormat("Usage: {0} <process id / part of process name / part of appdomain name>", _applicationConfiguration.EntryExe);
            Log.Warn("Will attach and print all exceptions.");
            Log.Warn("Limitation: Can only attach to .NET 3.5/4.0 programs running in 64-bit processes on 64-bit OSes");
        }

        private static void ConfigureLog4net()
        {
            Hierarchy hierarchy = (Hierarchy)LogManager.GetRepository();
            hierarchy.Root.RemoveAllAppenders(); /*Remove any other appenders*/

            ColoredConsoleAppender a = new ColoredConsoleAppender();
            PatternLayout pl = new PatternLayout();
            pl.ConversionPattern = "%d [%2%t] %-5p [%-10c]   %m%n%n";
            pl.ActivateOptions();
            a.Layout = pl;
            a.ActivateOptions();

            log4net.Config.BasicConfigurator.Configure(a);
        }

        private bool ShouldContinue()
        {
            return !Console.KeyAvailable;
        }


    }
}