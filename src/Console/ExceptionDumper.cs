﻿using System;
using System.Linq;
using System.Reflection;
using System.Threading;
using log4net;
using Microsoft.Samples.Debugging.MdbgEngine;

namespace Michaelkc.ExPrint
{
    /// <summary>
    /// This class is adapted from Microsoft sample code
    /// </summary>
    internal class ExceptionDumper
    {
        private readonly ApplicationConfiguration _configuration;
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private MDbgProcess _proc;
        private bool _canContinue = true;
        private WaitHandle _handle;
        private bool _isRunning = false;
        public ExceptionDumper(ApplicationConfiguration configuration)
        {
            _configuration = configuration;
        }

        public void Attach(int pid)
        {
            var debugger = new MDbgEngine();
            /* 
             * Specify which debug events we want to receive.
             * The underlying ICorDebug API will stop on all debug events.
             * The MDbgProcess object implements all of these callbacks, but only stops on a set of them
             * based off the Options settings.
             * 
             * See CorProcess.DispatchEvent and MDbgProcess.InitDebuggerCallbacks for more details.
             */
            debugger.Options.StopOnException = true;

            // Do 'debugger.Options.StopOnExceptionEnhanced = true;' to get additional exception notifications.
            _proc = debugger.Attach(pid);
        }

        public void DumpNextExceptionWithTimeout(TimeSpan timeout)
        {

            if (!_isRunning)
            {
                _handle = _proc.Go();
                _isRunning = true;
            }
            // Let the debuggee run and wait until it hits a debug event.
            bool exceptionThrown = _handle.WaitOne(timeout);
            if (!exceptionThrown)
            {
                Log.Debug(".");
                return;
            }
            _isRunning = false;
            object o = _proc.StopReason;

            // Process is now stopped. proc.StopReason tells us why we stopped.
            // The process is also safe for inspection.           
            var stopReason = o as ExceptionThrownStopReason;
            if (stopReason != null)
            {
                try
                {
                    var exceptionModel = ExceptionModel.CreateFromProcess(_proc, stopReason);
                    if (!_configuration.ExceptionsToIgnore.Contains(exceptionModel.ExceptionType, StringComparer.OrdinalIgnoreCase))
                    {
                        Log.ErrorFormat("{0}\n({1}) at function {2}\nSource:{3}",
                            exceptionModel.Exception, exceptionModel.StopReason, exceptionModel.Callstack, exceptionModel.Path);
                    }
                }
                catch (Exception ex)
                {
                    Log.Warn("Exception is thrown, but can't inspect it.");
                    Log.Warn(ex);
                    _canContinue = false;
                }
            }
        }

        public bool CanContinue()
        {
            return _proc.IsAlive && _canContinue;
        }

        public void Detach()
        {
            if (_proc != null && _proc.IsAlive)
            {
                Log.Info("Detaching...");
                _proc.AsyncStop().WaitOne();
                _proc.Detach();
                Log.Info("Detached.");
            }
        }
    }
}