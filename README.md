# ExPrint #

ExPrint is a .NET console application providing an easy way to attach to and get a streaming log of the first-chance exceptions occurring within arbitrary .NET processes. This can be very helpful if you lack the source code of the .NET code running within the process, or as a quick insight into configuration or dependency issues without having to enable error logging (which might even hide certain underlying exceptions; ExPrint will show them all). 

The heavy lifting in ExPrint is performed using the mdbg CLR Managed Debugger sample in library form. ExPrint itself is a run-of-the-mill console application with very simple usage, exposing the power of but shielding the user from the voodoo-esque commands of mdbg proper. Furthermore, ExPrint takes care not to pause the process being debugged any longer than necessary to extract the exception information, making it feasible to run on production processes if necessary.
Indeed, an earlier, cruder version of ExPrint has been used for this very purpose several times over the years, with no known bad side effects. 

### Limitations ###
* ExPrint can attach to 64-bit processes hosting .NET 4-4.5 on 64-bit operating systems. It should be able to attach to .NET 3.5 and .NET 4.6 processes as well, but I have only used it for 4.5 in recent history.
* ExPrint might have issues in your environments that it does not have in mine. Feel free to fork and fix.
* ExPrint currently includes no tests.

### Download ###
ExPrint has an AppVeyor build, from which a compiled version can be downloaded:
[![Build status](https://ci.appveyor.com/api/projects/status/bitbucket/michael_kaare_christensen/exprint?branch=master)](https://ci.appveyor.com/project/michael_kaare_christensen/exprint/build/artifacts)

### License ###
Copyright (c) 2016, Michael Christensen 
All rights reserved. 

Redistribution and use in source and binary forms, with or without 
modification, are permitted provided that the following conditions are met: 

 * Redistributions of source code must retain the above copyright notice, 
   this list of conditions and the following disclaimer. 
 * Redistributions in binary form must reproduce the above copyright 
   notice, this list of conditions and the following disclaimer in the 
   documentation and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY 
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY 
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
DAMAGE. 

### Contact ###
Exprint is developed by Michael Christensen ([@michaelkc](https://twitter.com/michaelkc/))
